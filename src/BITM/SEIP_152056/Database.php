<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 11:12 AM
 */

namespace App;
use PDO ,PDOException;

class Database
{

    public  $DBH;
    public  function __construct()
    {

        try{
            $this->DBH=new PDO('mysql:host=localhost;dbname=grading_system_b44','root','');
        } catch(\PDOException $error){

            echo $error->getMessage();

        }
    }

}