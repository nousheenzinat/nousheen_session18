<?php


namespace App;


class Result extends Database
{
    private $id;
    private $name;
    private $roll;
    private $markBangla;
    private $markEnglish;
    private $markMath;


    private $gradeBangla;
    private $gradeEnglish;
    private $gradeMath;


    public function  setdata($postData)
    {

        if (array_key_exists("id", $postData)) {


            $this->id = $postData['id'];
        }


        if (array_key_exists("name", $postData)) {


            $this->name = $postData['name'];
        }


        if (array_key_exists("roll", $postData)) {


            $this->roll = $postData['roll'];
        }


        if (array_key_exists("markBangla", $postData)) {


            $this->markBangla = $postData['$this->markBangla'];
            $this->gradeBangla = $this->convertMark2Grade($this->gradeBangla);
        }


        if (array_key_exists("markEnglish", $postData)) {


            $this->markEnglish = $postData['$this->markEnglish'];
            $this->gradeEnglish = $this->convertMark2Grade($this->gradeEnglish);
        }

        if (array_key_exists("markMath", $postData)) {


            $this->markMath = $postData['$this->markMath'];
            $this->gradeMath = $this->convertMark2Grade($this->gradeMath);
        }

        if (array_key_exists("gradeBangla", $postData)) {


            $this->gradeBangla = $postData['$this->gradeBangla'];
        }

        if (array_key_exists("gradeEnglish", $postData)) {


            $this->gradeEnglish = $postData['$this->gradeEnglish'];


        }

        if (array_key_exists("gradeMath", $postData)) {


            $this->gradeMath = $postData['$this->gradeMath'];
        }
    }

        public
        function convertMark2Grade($mark)
        {

            switch ($mark) {

                case $mark > 79:
                    return "A+";
                case $mark > 74:
                    return "A";
                case $mark > 69:
                    return "A-";
                case $mark > 64:
                    return "B+";
                case $mark > 59:
                    return "B";
                case $mark > 54:
                    return "B-";
                case $mark > 49:
                    return "C+";
                case $mark > 44:
                    return "C";
                default:
                    return "F";


            }
        }

        public function store()
        {

            $arrayData = array($this->name, $this->roll, $this->markBangla, $this->markEnglish, $this->markMath, $this->gradeBangla, $this->gradeEnglish, $this->gradeMath);
            $sql="INSERT into result(name,roll,mark_bangla,mark_english,mark_math,grade_bangla,grade_english,grade_math)VALUES (????????)";

          $sth=  $this->DBH->prepare($sql);
            $success = $sth->execute($arrayData);

            if($success){

                Message:: message("Data has been created successfully<br>");

            }else{


                Message:: message("Data has not been created successfully<br>");


            }

             utility::redirect("iinformationcollection.php");


        }
    }
